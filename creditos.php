<?php include 'includes/conexion.php' ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <?php require 'extensiones/head.php' ?>
    <title>Creditos</title>
</head>
<body style="background: #dfdfdf;"> 
    <?php require 'extensiones/navbar.php' ?>
    <div class="contenedor">
        <div class="titulo">
            <h2><strong>Creditos</strong></h2>
            <hr> 
            <div class="cuerpo">
                <h3>Grupo 10</h3>
                <h5>Fernando Bastias</h5>
                <h5>Marcelo Murillo</h5>
                <h5>Joaquin Padilla</h5>
                <h5>Joaquin Moreno</h5>
            </div>
        </div>
    </div>

    <?php require 'extensiones/scripts.php'?>
</body>
</html>