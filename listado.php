<?php 
 
include 'includes/conexion.php';

$query = "SELECT * FROM libro";
$consulta_usuarios = $conexion->query($query);


?>

<!DOCTYPE html>
<html lang="en">
<head>
    <?php require 'extensiones/head.php'?>
    <title>Listado de libros</title>
</head>
<body style="background: #dfdfdf;">
    <?php require 'extensiones/navbar.php'?>

    <div class="contenedor">
        <div class="table-responsive" style="padding: 1%">
            <div class="titulo">
                <h3><strong>Listado de libros</strong></h3>
                <hr>
            </div>
            <table class="table table-bordered" id="usuarios">
                <thead class="thead-light">
                    <tr>
                        <th scope="col">Id</th>
                        <th scope="col">Titulo</th>
                        <th scope="col">Autor</th>
                        <th scope="col">Año</th>
                        <th scope="col">Idioma</th>
                        <th scope="col">Opciones</th>
                    </tr>
                </thead>
                <tbody>
                <?php
                if($consulta_usuarios->num_rows > 0){
                    while ($usuarios = $consulta_usuarios->fetch_assoc()){                 
                ?>
                    <tr>
                        <td><?php echo $usuarios['id']?></td>
                        <td><?php echo $usuarios['titulo']?></td>
                        <td><?php echo $usuarios['autor']?></td>
                        <td><?php echo $usuarios['año']?></td>
                        <td><?php echo $usuarios['idioma']?></td>
                        <td>
                            <div class="row">
                                <div class="col-md-6">
                                    <a href="editar-libro.php?id=<?php echo $usuarios['id']?>" class="btn btn-block" style="background: #ff7777; color: white">Modificar</a>
                                </div>
                                <div class="col-md-6">
                                    <a href="eliminar-libro.php?id=<?php echo $usuarios['id']?>" class="btn btn-block" style="background: #ff0000; color: white">Eliminar</a>
                                </div>
                            </div>
                        </td>
                    </tr>
                <?php }} ?>
                </tbody>
            </table>
        </div>
        <div class="row">
            <div class="col-md-12">
                <?php 
                    if(!empty($_GET['error'])){
                        $respuesta = $_GET['error'];
                        $contenido = $_GET['contenido'];
                ?>
                <?php   if($respuesta=='modificado'){ ?>
                        <div class="col-md-12">
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                <strong><?php echo $contenido ?></strong>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>
                <?php   }else if($respuesta=='abort'){?>
                        <div class="col-md-12">
                            <div class="alert alert-warning alert-dismissible fade show" role="alert">
                                <strong><?php echo $contenido ?></strong>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>
                <?php   }else if($respuesta=='delete'){?>
                            <div class="col-md-12">
                                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                    <strong><?php echo $contenido ?></strong>
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            </div>
                    <?php 
                        }else if($respuesta=='nomod'){?>
                            <div class="col-md-12">
                                <div class="alert alert-warning alert-dismissible fade show" role="alert">
                                    <strong><?php echo $contenido ?></strong>
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            </div>
                    <?php
                        }
                    } 
                    ?>
            </div>
        </div>
    </div>
    <?php require 'extensiones/scripts.php'?>
</body>
</html>