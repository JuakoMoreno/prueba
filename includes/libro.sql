CREATE TABLE libro (
	id int primary key auto_increment,
    titulo varchar(64),
    autor varchar(64),
    año int,
    idioma varchar(32)
); 