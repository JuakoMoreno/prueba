<?php 

include 'includes/conexion.php';
$id = $_GET['id'];

$sql = "SELECT * FROM libro where id=$id";
$query_libro = $conexion->query($sql);


?><!DOCTYPE html>
<html lang="en"> 
<head>
    <?php require 'extensiones/head.php'?>
    <title>Eliminar Libro</title>
</head>
<body style="background: #dfdfdf;">
    <?php require 'extensiones/navbar.php'?>
    <div class="contenedor">
        <div class="titulo">
            <h2><strong>¿Estas seguro de eliminar el siguiente libro?:</strong></h2>
        </div>
        <div class="table-responsive" style="padding: 1%">
            <table class="table table-bordered" id="usuarios">
                <thead class="thead-light">
                    <tr>
                        <th scope="col">Id</th>
                        <th scope="col">Titulo</th>
                        <th scope="col">Autor</th>
                        <th scope="col">Año</th>
                        <th scope="col">Idioma</th>
                    </tr>
                </thead>
                <tbody>
                <?php
                if($query_libro->num_rows > 0){
                    while ($libro = $query_libro->fetch_assoc()){                 
                ?>
                    <tr>
                        <td><?php echo $libro['id']?></td>
                        <td><?php echo $libro['titulo']?></td>
                        <td><?php echo $libro['autor']?></td>
                        <td><?php echo $libro['año']?></td>
                        <td><?php echo $libro['idioma']?></td>
                        <?php $id = $libro['id']?>
                    </tr>
                <?php }} ?>
                </tbody>
            </table>
            <div class="row">
                <div class="col-md-6">
                    <?php $libro = $query_libro->fetch_assoc()?>
                    <a href="delete-libro.php?id=<?php echo $id?>" class="btn btn-block" style="background: green; color: white">SI</a>
                </div>
                <div class="col-md-6">
                    <a href="listado.php?error=abort&contenido=No se borró libro" class="btn btn-block" style="background: red; color: white">NO</a>    
                </div>
            </div>
        </div>
    </div>
    <?php require 'extensiones/scripts.php'?>
</body>
</html>