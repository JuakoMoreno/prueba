<?php 

include 'includes/conexion.php';
$id = $_GET['id'];

$sql = "SELECT * FROM libro WHERE id=$id";
$libro = $conexion->query($sql);

$libro = $libro->fetch_assoc();

?><!DOCTYPE html>
<html lang="en">
<head> 
    <?php require 'extensiones/head.php' ?>
    <title>Modificar Libro</title>
</head>
<body style="background: #dfdfdf;"> 
    <?php require 'extensiones/navbar.php' ?>
    <div class="contenedor">
        <div class="titulo">
            <h3><strong>Modificar Libro</strong></h3>
            <hr>
        </div>
        <div class="cuerpo">
            <form action="update-libro.php" method="POST">
                <div class="row">
                    <div class="col-md-1">
                        <span>Id:</span>
                            <div class="form-group">
                            <input class="form-control" type="text" id="id" name="id" value="<?php echo $libro['id'] ?>" readonly>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <span>Titulo:</span>
                            <div class="form-group">
                            <input class="form-control" type="text" id="titulo" name="titulo" value="<?php echo $libro['titulo'] ?>" required>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <span>Autor:</span>
                            <input class="form-control" type="text" id="autor" name="autor" value="<?php echo $libro['autor'] ?>" required >
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <span>Año:</span>
                            <input class="form-control" type="number" id="anio" name="anio" value="<?php echo $libro['año'] ?>" required >
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <span>Idioma:</span>
                            <input class="form-control" type="text" id="idioma" name="idioma" value="<?php echo $libro['idioma'] ?>" required >
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="form-group">
                            <button type="submit" class="btn btn-block" style="background: #ff7777; color: white">Modificar</button>
                        </div>
                    </div>
                    <div class="col-md-3">
                            <a href="listado.php?error=nomod&contenido=No se modificó libro" class="btn btn-block" style="background: red; color: white">Cancelar</a>   
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <?php require 'extensiones/scripts.php'?>
</body>
</html>