<?php include 'includes/conexion.php' ?> 

<!DOCTYPE html>
<html lang="en">
<head>
    <?php require 'extensiones/head.php' ?>
    <title>Registro de libros</title>
</head>
<body style="background: #dfdfdf;"> 
    <?php require 'extensiones/navbar.php' ?>
    <div class="contenedor">
        <div class="titulo">
            <h3><strong>Registro de libros</strong></h3>
            <hr>
        </div>
        <div class="cuerpo">
            <form action="insert-libro.php" method="POST">
                <div class="row">
                    <div class="col-md-4">
                        <span>Titulo:</span>
                            <div class="form-group">
                            <input class="form-control" type="text" id="titulo" name="titulo" required>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <span>Autor:</span>
                            <input class="form-control" type="text" id="autor" name="autor" required>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <span>Año:</span>
                            <input class="form-control" type="number" id="anio" name="anio" required>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <span>Idioma:</span>
                            <input class="form-control" type="text" id="idioma" name="idioma" required>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <button type="submit" class="btn btn-block" style="background: #ff7777; color: white">Registrar</button>
                        </div>
                    </div>
                </div>
            </form>
            <div class="row">
                <div class="col-md-12">
                    <?php 
                        if(!empty($_GET['error'])){
                            $respuesta = $_GET['error'];
                            $contenido = $_GET['contenido'];
                    ?>
                        <?php   if($respuesta=='vacio'){ ?>
                                <div class="col-md-12">
                                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                                        <strong><?php echo $contenido ?></strong>
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                </div>
                        <?php   } ?>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>

    <?php require 'extensiones/scripts.php'?>
</body>
</html>